ANAC stands for Assholes-Not-Allowed Clause.

* It is a short checklist that you can add to your open source licenses.
* The ```anac_license.md``` file contains a set of questions to disallow assholes to use your software. This way nazis, sexist people, TERF, etc. will not be allowed to use your open source code.

**Usage**: For example, you can decide that your software has a license [WTFPL](https://es.wikipedia.org/wiki/WTFPL) + ANAC or GPL + ANAC. I recommend, anyhow, that you copy paste the text at any place in the license you use so users realise as soon as possible.

PLEASE, add as many questions as you want through PR. This is just a draft for now, a proof of concept to introduce social responsability when spreading software.